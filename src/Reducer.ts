import { combineReducers } from 'redux';
import { requestSignInAction, requestSignUpAction, signOutAction } from './Actions';

const initialState = {
  user: {
    id: '',
    email: ''
  },
  signedIn: false
};

const issues = (state = initialState, action) => {
  switch (action.type) {

  case 'RESPONSE_SIGN_UP_ACTION': {
    const { user } = state;
    user.email = action.email;

    return {
      ...state,
      user,
      signedIn: true
    };
  }

  case 'RESPONSE_SIGN_IN_ACTION': {
    const { user } = state;
    user.id = action.uid;
    user.email = action.email;

    return {
      ...state,
      user,
      signedIn: true
    };
  }

  case 'SIGN_OUT_ACTION': {
    const { user } = state;

    user.id = '';
    user.email = '';

    return {
      ...state,
      user,
      signedIn: false
    };
  }

  default:
    return state;
  }
};

export const mapStateToProps = (state) => {
  return {
    user: state.issues.user,
    signedIn: state.issues.signedIn
  };
};

export const mapDispatchToProps = (dispatch) => {
  return {
    onSignIn: (email, password) => dispatch(requestSignInAction(email, password)),
    onSignUp: (email, password) => dispatch(requestSignUpAction(email, password)),
    onSignOut: () => dispatch(signOutAction())
  };
};

export default combineReducers({ issues: issues });
