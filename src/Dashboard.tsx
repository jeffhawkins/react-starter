import * as React from 'react';
import { connect } from 'react-redux';
import { mapDispatchToProps, mapStateToProps } from './Reducer';
import TextEditor from './TextEditor';

class Dashboard extends React.Component<any, any> {

  render() {
    return (
      <div className="dashboard">
        <p>React Starter Project</p>
        <TextEditor></TextEditor>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
