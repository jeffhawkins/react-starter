import * as React from 'react';

interface ITextEditorProps {
  options?: any;
  value?: string;
}

class TextEditor extends React.Component<any, any> {
  private editorId = Date.now().toString();

  handleOnBlur = (event: any) => {
    console.log('onBlur text: ' + this.getText());
    console.log('onBlur html: ' + this.getHtml());
  };

  handleOnKeyUp = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const content = document.getElementById(this.editorId);
    if (content) {
      for (let i = 0; i < content.childNodes.length; i++) {
        const node = content.childNodes.item(i);
        if (node && node.nodeType === node.TEXT_NODE) {
          console.log('text: ' + node.textContent);
        }
      }
    }
  };

  handlePaste = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const pasteText = event.clipboardData.getData('text');
    console.log('paste: ' + pasteText);

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }
    if (!selection.rangeCount && this.isSelectionValid(selection.anchorNode)) {
      return false;
    }

    selection.getRangeAt(0).insertNode(document.createTextNode(pasteText));
  };

  handleOnBold = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }

    const selectedText = selection.toString();
    selection.deleteFromDocument();

    const bold = document.createElement('b');
    bold.appendChild(document.createTextNode(selectedText));

    selection.getRangeAt(0).insertNode(bold);
  };

  handleOnItalic = (event: any) => {
    event.preventDefault();
    event.stopPropagation();

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }

    const selectedText = selection.toString();
    selection.deleteFromDocument();

    const italic = document.createElement('i');
    italic.appendChild(document.createTextNode(selectedText));

    selection.getRangeAt(0).insertNode(italic);
  };

  handleOnPlaceholder = (event: any) => {
    console.log('placeholder');
    event.preventDefault();
    event.stopPropagation();

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }

    const span = document.createElement('span');
    span.classList.add('placeholder');
    span.setAttribute('data-type', 'placeholder');
    span.setAttribute('contenteditable', 'false');
    span.appendChild(document.createTextNode('[CLIENTNAME]'));

    selection.getRangeAt(0).insertNode(span);
    selection.removeAllRanges();
  };

  handleOnHorizontalRule = (event: any) => {
    console.log('hr');
    event.preventDefault();
    event.stopPropagation();

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }

    const hr = document.createElement('hr');
    hr.setAttribute('data-type', 'hr');

    selection.getRangeAt(0).insertNode(hr);
  };

  handleOnLink = (event: any) => {
    console.log('link');
    event.preventDefault();
    event.stopPropagation();

    const selection = window.getSelection();
    if (!this.isSelectionValid(selection)) {
      return false;
    }

    const a = document.createElement('a');
    a.setAttribute('data-type', 'link');
    a.setAttribute('href', 'http://google.com');
    a.appendChild(document.createTextNode('Google'));

    selection.getRangeAt(0).insertNode(a);
  };

  // Verify the selection or insertion point is within the TextEditor.
  isSelectionValid = (selection: any) => {
    if (!selection.rangeCount) {
      return false;
    }

    let parent = selection.anchorNode as Element;

    while (parent) {
      if (parent.id === this.editorId) {
        return true;
      }

      parent = parent.parentElement;
    }

    return false;
  };

  getText = () => {
    let text = '';
    const content = document.getElementById(this.editorId);
    if (content) {
      for (let i = 0; i < content.childNodes.length; i++) {
        const node = content.childNodes.item(i);
        if (node) {
          text += node.textContent;
          text += '\n';
        }
      }
    }

    return text;
  };

  getHtml = () => {
    let html = '';
    const content = document.getElementById(this.editorId);
    if (content) {
      html = content.innerHTML;
    }

    return html;
  };

  componentDidMount() {
    const content = document.getElementById(this.editorId);

    if (this.props.value) {
      if (content) {
        content.innerHTML = this.props.value;
      }
    }
    else {
      // Start with paragraph with a break in it.
      if (content) {
        content.innerHTML = '<p><br/></p>';
      }
    }
  }

  render() {
    return (
      <div className="texteditor">
        <div className="texteditor-toolbar">
          <input type="button" value="Bold" onClick={(event) => this.handleOnBold(event)}/>
          <input type="button" value="Italic" onClick={(event) => this.handleOnItalic(event)}/>
          <input type="button" value="Placeholder" onClick={(event) => this.handleOnPlaceholder(event)}/>
          <input type="button" value="HR" onClick={(event) => this.handleOnHorizontalRule(event)}/>
          <input type="button" value="link" onClick={(event) => this.handleOnLink(event)}/>
        </div>
        <div className="texteditor-container">
          <div id={this.editorId} className="texteditor-content"
            contentEditable
            onBlur= {(event) => this.handleOnBlur(event)}
            onKeyUp={(event) => this.handleOnKeyUp(event)}
            onPaste={(event) => this.handlePaste(event)}>
          </div>
        </div>
      </div>
    );
  }
}

export default TextEditor;
