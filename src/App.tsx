import 'font-awesome/css/font-awesome.css';
import * as React from 'react';
import { connect } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import './App.scss';
import Dashboard from './Dashboard';
import { mapDispatchToProps, mapStateToProps } from './Reducer';

interface IAppProps {
  signedIn: boolean;
  user: any;
}

class App extends React.Component<IAppProps, any> {
  render() {
    return (
      <Router>
        <div className="app-container">
          <header>
            <div className="title">React Starter</div>
            <div className="user-name">John Doe</div>
          </header>
          <Switch>
            <Route exact path="/" component={Dashboard}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
