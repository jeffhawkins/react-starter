import * as firebase from 'firebase';
import { signIn, signUp } from './DatabaseHelper';

export const requestSignInAction = (email: string, password: string) => {
  return (dispatch, getState) => {
    signIn(email, password)
      .then((response: firebase.auth.UserCredential) => {
        dispatch(responseSignInAction(response.user.uid, email));
      })
      .catch((response) => {
        // dispatch(showErrorMessageAction('Invalid email or password.'));
        console.log('Invalid email or password.');
      });
  };
};

export const responseSignInAction = (uid: string, email: string) => {
  return {
    type: 'RESPONSE_SIGN_IN_ACTION',
    uid,
    email
  };
};

export const requestSignUpAction = (email: string, password: string) => {
  return (dispatch, getState) => {
    signUp(email, password)
      .then((response) => {
        dispatch(responseSignUpAction(email));
      })
      .catch((response) => {
        // dispatch(showErrorMessageAction('Invalid email or password.'));
        console.log('Invalid email or password.');
      });
  };
};

export const responseSignUpAction = (email: string) => {
  return {
    type: 'RESPONSE_SIGN_UP_ACTION',
    email
  };
};

export const signOutAction = () => {
  return {
    type: 'SIGN_OUT_ACTION'
  };
};
