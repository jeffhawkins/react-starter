import * as firebase from 'firebase';
import 'firebase/firestore';

const ISSUES = 'issues';

// eslint-disable-next-line
let userId = null;

let database: firebase.firestore.Firestore;

const init = () => {
  firebase.initializeApp({
    apiKey: 'AIzaSyDQPp3CDALP5c-663Sc1JYCvrwTfk2K3xw',
    authDomain: 'realtor-notes.firebaseapp.com',
    databaseURL: 'https://realtor-notes.firebaseio.com',
    projectId: 'realtor-notes',
    storageBucket: 'realtor-notes.appspot.com',
    messagingSenderId: '749753163667'
  });

  database = firebase.firestore();
  database.settings({timestampsInSnapshots: true});
};

// export const uploadFile = (file: FileList, metadata: any) => {
//   const f = file[0];
//   if (f.size > 0) {
//     metadata.contentType = f.type;
//     const storage = firebase.storage().ref().child('uploads/' + f.name);
//     storage.put(f, metadata).then(() => {
//       console.log('Uploaded file.');
//     }).catch((error) => {
//       console.log('Upload error: ' + error.code);
//     });
//   }
// };

export const signIn = (email: string, password: string) => {
  return new Promise((resolve, reject) => {
    firebase.auth().signInWithEmailAndPassword(email.trim(), password.trim())
      .then((response: firebase.auth.UserCredential) => {
        userId = response.user.uid;
        resolve(response);
      })
      .catch((error) => {
        console.log('fail: ' + error);
        reject(error);
      });
  });
};

export const signUp = (email: string, password: string) => {
  return new Promise((resolve, reject) => {
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((response: firebase.auth.UserCredential) => {
        userId = response.user.uid;
        resolve(response);
      })
      .catch((error) => {
        console.log('fail: ' + error);
        reject(error);
      });
  });
};

// Return promise to caller.
// export const update = (issue: Issue) => {
//   issue.modifiedBy = userId;
//   issue.updated = new Date().toISOString();
//   return database.collection(ISSUES).doc(issue.id).update(issue);
// };

// export const insert = (issue: Issue) => {
//   const ref: firebase.firestore.DocumentReference = database.collection(ISSUES).doc();

//   issue.modifiedBy = userId;
//   issue.created = new Date().toISOString();
//   issue.updated = new Date().toISOString();
//   issue.id = ref.id;

//   // Return the promise to the caller.
//   return ref.set(issue);
// };

// const fetchIssuesByStatus = (status: string) => {
//   return new Promise((resolve, reject) => {
//     let query: firebase.firestore.Query = database.collection(ISSUES);

//     if (status) {
//       query = query.where('status', '==', status);
//     }

//     query.orderBy('updated', 'desc')
//       .get()
//       .then((snapshot) => {
//         const issues: Issue[] = [];

//         snapshot.forEach((doc: firebase.firestore.QueryDocumentSnapshot) => {
//           const issue: Issue = doc.data() as Issue;
//           issue.id = doc.id;
//           issues.push(issue);
//         });

//         resolve(issues);
//       })
//       .catch((error) => {
//         console.log('fail: ' + error);
//         reject(error);
//       });
//   });
// };

init();
